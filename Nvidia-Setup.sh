#!/bin/bash
# -*- coding: utf-8 -*-

######################################################################################################################################
#                                               Nvidia DRIVER/CUDA setup
#
# Use this script to install Nvidia Driver & CUDA.
#
# Change History
# 11/11/2021 - Walddys Emmanuel Dorrejo Céspedes        Original Code. script to setup CUDA created.
# 29/11/2021 - Walddys Emmanuel Dorrejo Céspedes        Adding Nvidia Driver Setup from official repo.
# 10/07/2022 - Walddys Emmanuel Dorrejo Céspedes        Re-structuration for files and publishing v1.
# 11/10/2022 - Walddys Emmanuel Dorrejo Céspedes        Update to Gl06
#
######################################################################################################################################
#
# Author: Walddys Emmanuel Dorrejo Céspedes
# Email: dev.dorrejo@gmail.com (use subject: nvidia-driver-cuda-scripts)
# Telegram: https://t.me/SySDorrejo
#
######################################################################################################################################

if [[ $(id -u) != 0 ]]; then
    clear
    echo "Execute like root or sudo".
    sleep 2
    clear
    exit 0
fi

if nvidia-smi; then
    NVIDIA_SMI_VERSION=$(nvidia-smi | grep -i "cuda version:" | awk '{print $9}')
    CUDA_VERSION="${NVIDIA_SMI_VERSION//./-}"
    echo "$CUDA_VERSION"

    zypper --gpg-auto-import-keys -p 100 ar -n CUDA -cfg https://developer.download.nvidia.com/compute/cuda/repos/opensuse15/x86_64/cuda-opensuse15.repo
    zypper in -y cuda-nvcc-"${CUDA_VERSION}"
fi

case "${SHELL}" in
/bin/bash)
    if grep -E "^export PATH" "$HOME"/.bashrc; then
        sed -i.old "s_^export PATH.*_&:/usr/local/cuda-${NVIDIA_SMI_VERSION}_" "$HOME"/.bashrc # SED way to append go path to the existing user "export PATH"
    else
        echo "export PATH=${PATH:+$PATH}:/usr/local/cuda-${NVIDIA_SMI_VERSION}" >>"$HOME"/.bashrc
    fi

    if grep -E "^export LD_LIBRARY_PATH" "$HOME"/.bashrc; then
        sed -i.old "s_^export LD_LIBRARY_PATH.*_&:/usr/local/cuda-${NVIDIA_SMI_VERSION}/lib64_" "$HOME"/.bashrc # SED way to append go path to the existing user "export LD_LIBRARY_PATH"
    else
        echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}:/usr/local/cuda-${NVIDIA_SMI_VERSION}/lib64" >>"$HOME"/.bashrc
    fi
    ;;

/bin/zsh)
    if grep -E "^export PATH" "$HOME"/.zshrc; then
        sed -i.old "s_^export PATH.*_&:/usr/local/cuda-${NVIDIA_SMI_VERSION}_" "$HOME"/.zshrc # SED way to append go path to the existing user "export PATH"
    else
        echo "export PATH=${PATH:+$PATH}:/usr/local/cuda-${NVIDIA_SMI_VERSION}" >>"$HOME"/.zshrc
    fi

    if grep -E "^export LD_LIBRARY_PATH" "$HOME"/.zshrc; then
        sed -i.old "s_^export LD_LIBRARY_PATH.*_&:/usr/local/cuda-${NVIDIA_SMI_VERSION}/lib64_" "$HOME"/.zshrc # SED way to append go path to the existing user "export LD_LIBRARY_PATH"
    else
        echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}:/usr/local/cuda-${NVIDIA_SMI_VERSION}/lib64" >>"$HOME"/.zshrc
    fi
    ;;
*)
    echo "Unknown SHELL"
    exit 254
    ;;
esac

#########################################################################################
# With this setting, the NVIDIA GPU driver will allow the GPU to go into its lowest power state when no applications are running that use the nvidia driver stack. Whenever an
# application requiring NVIDIA GPU access is started, the GPU is put into an active state. When the application exits, the GPU is put into a low power state.
#
# Additionally, the NVIDIA driver will actively monitor GPU usage while applications using the GPU are running. When the applications have not used the GPU for a short period,
# the driver will allow the GPU to be powered down. As soon as the application starts using the GPU, the GPU is reactivated.
#
# It is important to note that the NVIDIA GPU will remain in an active state if it is driving a display. In this case, the NVIDIA GPU will go to a low power state only when the X
# configuration option HardDPMS is enabled and the display is turned off by some means - either automatically due to an OS setting or manually using commands like xset.
#
# Similarly, the NVIDIA GPU will remain in an active state if a CUDA application is running.
#########################################################################################
cat >/etc/modprobe.d/09-nvidia.conf <<EOF
options nvidia NVreg_DynamicPowerManagement=0x02
EOF
